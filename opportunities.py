#!/usr/bin/env python
from __future__ import print_function

import gevent
from gevent import monkey; monkey.patch_all()
import logging
import argparse
import time
import sys
import curses

from exchanges.poloniex import Poloniex as poloniex
from exchanges.bittrex import Bittrex as bittrex
from terminaltables import AsciiTable
from datetime import datetime


try:
    # For Python 3+
    from configparser import ConfigParser, NoSectionError
except ImportError:
    # Fallback to Python 2.7
    from ConfigParser import ConfigParser, NoSectionError

# Pair Strings for accessing API responses
PAIRS = [('ETH', 'CVC'), ('ETH', 'ETC'), ('ETH', 'STEEM'), ('ETH', 'GNT'),
         ('ETH', 'ZEC'), ('ETH', 'OMG'), ('ETH', 'REP'), ('ETH', 'GNO'),
         ('BTC', 'ETH'), ('BTC', 'XMR'), ('BTC', 'LTC'), ('BTC', 'XRP'),
         ('BTC', 'ZEC'), ('BTC', 'SC')]


class Terminal(object):
    """Represents the window on the user terminal for monitoring coin pairs.

    A Terminal creates a data table for each base currency, with a list for its
    target coins. This data table is used to monitor in real-time the arbitrage
    opportunities for each pair. The results will be added on the terminal
    window, which is refreshed every 2 seconds.
    """

    EMPTY_VALUE = '  -  '

    def __init__(self):
        self._init_coin_indexes_dict()

        self._table_data = {
            'ETH': [],
            'BTC': []
        }
        self._init_data_table('ETH')
        self._init_data_table('BTC')

    def _init_coin_indexes_dict(self):
        """Creates dictionary with coin pairs and corresponding table index.

        After executing the function the _table_indexes property will look like
        {
            'ETH': {
                'CVC':   [1, 1],
                'ETC':   [2, 1],
            },
            'BTC': {
                'ETH':   [1, 1]
            }
        }
        """
        self._table_indexes = {}
        for base, target in sorted(PAIRS, key=lambda x: x[0]):
            if base not in self._table_indexes:
                # New base found
                j = 1
                self._table_indexes[base] = {}
            # Ignore duplicate pairs
            if target not in self._table_indexes[base]:
                self._table_indexes[base][target] = [j, 1]
                j += 1

    def get_index_for_pair(self, base, target):
        """Gets the table index for give base-target pair.

        Args:
            base: The base currency (e.g ETH).
            target: The target currency (e.g. CVC).

        Returns:
            The table index for give base-target pair.
        """
        index = self._table_indexes[base][target]
        if index:
            return index
        return None

    @property
    def table_data(self):
        return self._table_data

    @table_data.setter
    def table_data(self, table_data):
        self._table_data = table_data

    def _init_data_table(self, base):
        """Creates table for monitoring arbitrage on coin pairs for base coin.

        When table is created, each pair will be given a default value.

        Args:
            base: The base currency (e.g ETH).
        """
        target_dict = self._table_indexes[base]
        coin_indexes = sorted(target_dict.items(), key=lambda x: x[1][0])
        self._table_data[base].append(['Coin', 'Arbitrage', 'Max'])
        for coin, _ in coin_indexes:
            self._table_data[base].append([coin, self.EMPTY_VALUE, self.EMPTY_VALUE])

    def _print_table(self, window):
        """Prints all monitoring tables.

        Converts each table for each base currency into Ascii format and along
        with a header table (with the exchanges and a timer) adds them to the
        terminal window, which is refreshed every 1 second.

        Args:
            window: The terminal window hook passed from python curses.
        """
        while True:
            # Header table
            t = datetime.now()
            table_data = [[
                'Bittrex', 'Poloniex', ' ' * 10, t.strftime('%H:%M:%S')
            ]]
            table = AsciiTable(table_data, 'Exchanges')
            j = 0
            for data in table.table.split('\n'):
                window.addstr(1 + j, 30, data)
                j += 1

            # ETH monitoring table
            table_eth = AsciiTable(self._table_data['ETH'], 'ETH')
            table_eth.inner_heading_row_border = False
            table_eth.justify_columns = {0: 'center', 1: 'center'}
            table_eth.inner_row_border = True

            j = 0
            for data in table_eth.table.split('\n'):
                window.addstr(10 + j, 10, data)
                j += 1

            # BTC monitoring table
            table_btc = AsciiTable(self._table_data['BTC'], 'BTC')
            table_btc.inner_heading_row_border = False
            table_btc.justify_columns = {0: 'center', 1: 'center'}
            table_btc.inner_row_border = True

            j = 0
            for data in table_btc.table.split('\n'):
                window.addstr(10 + j, 60, data)
                j += 1

            window.refresh()
            time.sleep(1)

    def tprint(self):
        curses.wrapper(self._print_table)


class OpportunityBot(object):
    def __init__(self):
        self.setup_arg_parser()
        self.setup_logger()
        self.load_config()

    def setup_arg_parser(self):
        """Sets up argument parser"""

        parser = argparse.ArgumentParser(
            description='Poloniex/Bittrex Arbitrage Discovery Bot')
        parser.add_argument(
            '-r',
            '--rate',
            default=1.01,
            type=float,
            required=False,
            help='min arbitrage, 1.01 is 1 % price difference [default: 1.01]')
        parser.add_argument(
            '-i',
            '--interval',
            default=1,
            type=int,
            required=False,
            help='seconds to sleep between loops [default: 1]')
        parser.add_argument(
            '-c',
            '--config',
            default='arbbot.conf',
            type=str,
            required=False,
            help='config file [default: arbbot.conf]')
        parser.add_argument(
            '-l',
            '--logfile',
            default='opbot.log',
            type=str,
            required=False,
            help='file to output log data to [default: opbot.log]')
        parser.add_argument(
            '-v',
            '--verbose',
            action='store_true',
            required=False,
            help='enables extra console messages (for debugging)')
        parser.add_argument(
            '-o',
            '--output',
            default='log',
            required=False,
            help='logs output or creates monitoring table on terminal')
        self.args = parser.parse_args()

    def setup_logger(self):
        """Sets up bot logger"""

        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        # Create file handler and set level to debug
        fh = logging.FileHandler(
            self.args.logfile, mode='a', encoding=None, delay=False)
        fh.setLevel(logging.DEBUG)
        # Create formatter
        formatter = logging.Formatter(
            '%(asctime)s - %(levelname)s - %(message)s')
        # Add formatter to handlers
        fh.setFormatter(formatter)
        # Add handlers to logger
        self.logger.addHandler(fh)

    def load_config(self):
        """Loads bot configuration"""

        config = ConfigParser()
        try:
            config.read(self.args.config)
            polo_key = config.get('ArbBot', 'poloniexKey')
            polo_secret = config.get('ArbBot', 'poloniexSecret')
            btrx_key = config.get('ArbBot', 'bittrexKey')
            btrx_secret = config.get('ArbBot', 'bittrexSecret')
        except NoSectionError:
            self.logger.warning('No Config File Found! Running in Drymode!')
            self.args.dryrun = True
            polo_key = 'POLONIEX_API_KEY'
            polo_secret = 'POLONIEX_API_SECRET'
            btrx_key = 'BITTREX_API_KEY'
            btrx_secret = 'BITTREX_API_SECRET'
            config.add_section('ArbBot')
            config.set('ArbBot', 'poloniexKey', polo_key)
            config.set('ArbBot', 'poloniexSecret', polo_secret)
            config.set('ArbBot', 'bittrexKey', btrx_key)
            config.set('ArbBot', 'bittrexSecret', btrx_secret)
        try:
            with open(self.args.config, 'w') as configfile:
                config.write(configfile)
        except IOError:
            self.logger.error('Failed to create and/or write to {}'.format(
                self.args.config))

        self.exchange_config = {
            'poloniexKey': polo_key,
            'poloniexSecret': polo_secret,
            'bittrexKey': btrx_key,
            'bittrexSecret': btrx_secret
        }

    def quit(self):
        self.logger.info('KeyboardInterrupt, quitting!')
        sys.exit()

    def search(self, pair):
        """Searches for arbitrage opportunities in a given pair.

        It continuously queries Bittrex and Poloniex exchanges and compares
        asks and bids for the given pair, to find arbitrage opportunities.

        Args:
            pair: The crypto pair to check for arbitrage.
        """

        base_currency, target_currency = pair

        btrx_pair = '{0}-{1}'.format(base_currency, target_currency)
        polo_pair = '{0}_{1}'.format(base_currency, target_currency)

        # Log Startup Settings
        self.logger.info('Arb Pair: {} | Rate: {} | Interval: {}'.format(
            btrx_pair, self.args.rate, self.args.interval))

        # Main Loop
        while True:
            # Query Poloniex Prices
            current_values = self.polo_api.get_ticker()

            # Query Bittrex Prices
            summary = self.btrx_api.get_market_summary(btrx_pair)

            if (not current_values or not summary):
                time.sleep(self.args.interval)
                continue

            try:
                polo_bid = float(current_values[polo_pair]["highestBid"])
                polo_ask = float(current_values[polo_pair]["lowestAsk"])
                btrx_ask = summary[0]['Ask']
                btrx_bid = summary[0]['Bid']
            except:
                continue

            # Print Prices
            self.oprint(
                '\nPair: {}'.format(btrx_pair) +
                '\nASKS: Poloniex @ {:.8f}'.format(polo_ask) +
                ' | {:.8f} @ Bitrex'.format(btrx_ask) +
                '\nBIDS: Poloniex @ {:.8f}'.format(polo_bid) +
                ' | {:.8f} @ Bitrex'.format(btrx_bid))

            if polo_ask < btrx_bid:
                self.notify(polo_ask, btrx_bid, pair)
            elif btrx_ask < polo_bid:
                self.notify(btrx_ask, polo_bid, pair)

            time.sleep(self.args.interval)

    def notify(self, ask, bid, pair):
        """Checks if arbitrage is high enough and updates Terminal.

        Args:
            ask: The lowest ask price in the order book of the buy exchange.
            bid: The highest bid price in the order book of the sell exchange.
            pair: The crypto pair to check for arbitrage.
        """
        arbitrage = bid / ask
        self.update_terminal(arbitrage, pair)
        self.oprint('OPPORTUNITY - PAIR: {}'.format(pair) +
                    ' | RATE: {}'.format(arbitrage))

        # Return if minumum arbitrage percentage is not met
        if arbitrage <= self.args.rate:
            return

        self.logger.info('OPPORTUNITY - BUY PAIR: {}'.format(pair) +
                         ' | RATE: {}'.format(arbitrage))
        self.oprint(
            'BUY PAIR: {}'.format(pair) + ' | RATE: {}'.format(arbitrage))

    def update_terminal(self, arbitrage, pair):
        """Updates price of pair in data table of monitoring terminal

        Args:
            arbitrage: The detected arbitrage.
            pair: The crypto pair to check for arbitrage.
        """

        if self.args.output == 'monitor':
            base, target = pair
            index = self.terminal.get_index_for_pair(base, target)
            if index:
                row, col = index
                arbstr = "{0:.3f}".format(arbitrage)
                self.terminal.table_data[base][row][col] = arbstr
                max_value = self.terminal.table_data[base][row][col + 1]
                if max_value == self.terminal.EMPTY_VALUE or float(arbitrage) > float(max_value):
                    self.terminal.table_data[base][row][col + 1] = arbstr

    def oprint(self, s):
        if self.args.output == 'log':
            print(s)


def main():
    """Main function to run

    Initializes OpportunityBot and Terminal classes, loads configuration file,
    creates exchange API objects and spawns asynchronous opportunity searches
    for every pair listed in PAIRS.
    """

    # Initialize OpportunityBot and Monitoring Terminal
    opbot = OpportunityBot()
    t = Terminal()
    opbot.terminal = t

    # Load Config File
    config = opbot.exchange_config
    polo_key = config['poloniexKey']
    polo_secret = config['poloniexSecret']
    btrx_key = config['bittrexSecret']
    btrx_secret = config['bittrexSecret']

    # Create Exchange API Objects
    opbot.btrx_api = bittrex(btrx_key, btrx_secret)
    opbot.polo_api = poloniex(polo_key, polo_secret)

    # Spawn asynchronous jobs
    jobs = [gevent.spawn(opbot.search, pair) for pair in PAIRS]
    if opbot.args.output == 'monitor':
        jobs.append(gevent.spawn(opbot.terminal.tprint))

    gevent.wait(jobs)


if __name__ == "__main__":
    main()
