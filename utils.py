import sys
import logging

class errorHandler(object):
    def __init__(self, error_msg):
        self.error_msg = error_msg
        self.init_logger()

    def init_logger(self):
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)

    def __call__(self, f):
        def wrapped_f(*args):
            try:
                res = f(*args)
                return res
            except KeyboardInterrupt:
                print('KeyboardInterrupt, quitting!')
                sys.exit()
            except Exception as e:
                # Fail Gracefully
                self.logger.error(self.error_msg)
            return False
        return wrapped_f
