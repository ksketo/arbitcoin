import json
import time
import hmac
import hashlib
from utils import errorHandler

try:
    # For Python 3.0 and later
    from urllib.request import urlopen, Request
    from urllib.parse import urlencode
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen, Request
    from urllib import urlencode


def createTimeStamp(datestr, format="%Y-%m-%d %H:%M:%S"):
    return time.mktime(time.strptime(datestr, format))


class Poloniex:
    def __init__(self, APIKey, Secret):
        self.name = 'Poloniex'
        self.APIKey = APIKey
        self.Secret = Secret
        self.fees = 0.25

    def set_pair(self, pair):
        self._pair = pair

    def post_process(self, before):
        after = before

        # Add timestamps if there isnt one but is a datetime
        if ('return' in after):
            if (isinstance(after['return'], list)):
                for x in xrange(0, len(after['return'])):
                    if (isinstance(after['return'][x], dict)):
                        if ('datetime' in after['return'][x]
                                and 'timestamp' not in after['return'][x]):
                            after['return'][x]['timestamp'] = float(
                                createTimeStamp(
                                    after['return'][x]['datetime']))

        return after

    def api_query(self, command, req={}):

        if (command == "returnTicker" or command == "return24Volume"):
            ret = urlopen(
                Request('https://poloniex.com/public?command=' + command))
            return json.loads(ret.read())
        elif (command == "returnOrderBook"):
            ret = urlopen(
                Request('https://poloniex.com/public?command=' + command +
                        '&currencyPair=' + str(req['currencyPair'])))
            return json.loads(ret.read())
        elif (command == "returnMarketTradeHistory"):
            ret = urlopen(
                Request('https://poloniex.com/public?command=' +
                        "returnTradeHistory" + '&currencyPair=' +
                        str(req['currencyPair'])))
            return json.loads(ret.read())
        else:
            req['command'] = command
            req['nonce'] = int(time.time() * 1000)
            post_data = urlencode(req)

            sign = hmac.new(self.Secret, post_data, hashlib.sha512).hexdigest()
            headers = {'Sign': sign, 'Key': self.APIKey}

            ret = urlopen(
                Request('https://poloniex.com/tradingApi', post_data, headers))
            jsonRet = json.loads(ret.read())
            return self.post_process(jsonRet)

    @errorHandler('Failed to get Poloniex Ticker, Restarting Loop')
    def get_ticker(self):
        return self.api_query("returnTicker")

    def return_24volume(self):
        return self.api_query('return24Volume')

    def _get_orderbook(self, currencyPair):
        return self.api_query('returnOrderBook', {
            'currencyPair': currencyPair
        })

    @errorHandler('Failed to get Poloniex Asks, skipping order attempt')
    def _get_top_order(self, currencyPair, order_type):
        orderbook = self._get_orderbook(currencyPair)
        return orderbook[order_type][0][1]

    def get_lowest_ask_size(self):
        if not self._pair:
            return False
        currencyPair = self._pair
        return self._get_top_order(currencyPair, 'asks')

    def get_highest_bid_size(self):
        if not self._pair:
            return False
        currencyPair = self._pair
        return self._get_top_order(currencyPair, 'bids')

    def get_lowest_ask_price(self):
        if not self._pair:
            return False
        current_values = self.get_ticker()
        if not current_values:
            return False
        return float(current_values[self._pair]['lowestAsk'])

    def get_highest_bid_price(self):
        if not self._pair:
            return False
        current_values = self.get_ticker()
        if not current_values:
            return False
        return float(current_values[self._pair]['highestBid'])

    def get_market_trade_history(self, currencyPair):
        return self.api_query('returnMarketTradeHistory', {
            'currencyPair': currencyPair
        })

    @errorHandler('Failed to Query Poloniex for Balance Data, Restarting Loop')
    def get_balances(self):
        """Returns all of your balances

        Outputs:
            {'BTC': '0.59098578', 'LTC': '3.31117268', ... }
        """
        print('in poloniex.get_balances()')
        print(self.api_query('returnBalances'))
        return self.api_query('returnBalances')

    def get_open_orders(self, currencyPair):
        """Returns your open orders for a given market

        Args:
            currencyPair: The currency pair e.g. 'BTC_XCP'
        Outputs:
            orderNumber:  The order number
            type:         sell or buy
            rate:         Price the order is selling or buying at
            Amount:       Quantity of order
            total:        Total value of order (price * quantity)
        """
        return self.api_query('returnOpenOrders', {
            'currencyPair': currencyPair
        })

    def get_trade_history(self, currencyPair):
        """Returns your trade history for a given market

        Args:
            currencyPair: The currency pair e.g. "BTC_XCP"
        Outputs:
            date:         Date in the form: "2014-02-19 03:44:59"
            rate:         Price the order is selling or buying at
            amount:       Quantity of order
            total:        Total value of order (price * quantity)
            type:         sell or buy
        """
        return self.api_query('returnTradeHistory', {
            "currencyPair": currencyPair
        })

    def buy_limit(self, amount, rate):
        """ Places a *limit* buy order in a given market

        Required POST parameters are "currencyPair", "rate", and "amount".
        If successful, the method will return the order number.

        Args:
            currencyPair: The curreny pair
            rate:         Price the order is buying at
            amount:       Amount of coins to buy
        Outputs:
            orderNumber:  The order number
        """
        if not self._pair:
            return False
        currencyPair = self._pair

        return self.api_query('buy', {
            "currencyPair": currencyPair,
            "rate": rate,
            "amount": amount
        })

    def sell_limit(self, amount, rate):
        """Places a *limit* sell order in a given market.

        Required POST parameters are "currencyPair", "rate", and "amount".
        If successful, the method will return the order number.

        Args:
            currencyPair: The curreny pair
            rate:         Price the order is selling at
            amount:       Amount of coins to sell
        Outputs:
            orderNumber:  The order number
        """
        if not self._pair:
            return False
        currencyPair = self._pair

        return self.api_query('sell', {
            "currencyPair": currencyPair,
            "rate": rate,
            "amount": amount
        })

    def cancel(self, currencyPair, orderNumber):
        """Cancels an order you have placed in a given market.

        Required POST parameters are "currencyPair" and "orderNumber".

        Args:
            currencyPair: The curreny pair
            orderNumber:  The order number to cancel
        Outputs:
            succes:       1 or 0
        """
        return self.api_query('cancelOrder', {
            "currencyPair": currencyPair,
            "orderNumber": orderNumber
        })

    def withdraw(self, currency, amount, address):
        """Place a withdrawal for a given currency

        Immediately places a withdrawal for a given currency, with no email
        confirmation. In order to use this method, the withdrawal privilege
        must be enabled for your API key.
        Required POST parameters are "currency", "amount", and "address".
        Sample output: {"response":"Withdrew 2398 NXT."}

        Args:
            currency:     The currency to withdraw
            amount:       The amount of this coin to withdraw
            address:      The withdrawal address
        Outputs:
            response:     Text containing message about the withdrawal
        """
        return self.api_query('withdraw', {
            "currency": currency,
            "amount": amount,
            "address": address
        })
