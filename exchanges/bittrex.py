#!/usr/bin/env python
import urllib
import json
import time
import hmac
import hashlib
from utils import errorHandler

try:
    # Fallback to Python 2.7
    import urllib2
except ImportError:
    # For Python 3+
    pass


class Bittrex(object):

    def __init__(self, key, secret):
        self.name = 'Bittrex'
        self.key = key
        self.secret = secret
        self.public = [
            'getmarkets', 'getcurrencies', 'getticker', 'getmarketsummaries',
            'getmarketsummary', 'getorderbook', 'getmarkethistory'
        ]
        self.market = [
            'buylimit', 'buymarket', 'selllimit', 'sellmarket', 'cancel',
            'getopenorders'
        ]
        self.account = [
            'getbalances', 'getbalance', 'getdepositaddress', 'withdraw',
            'getorder', 'getorderhistory', 'getwithdrawalhistory',
            'getdeposithistory'
        ]
        self.fees = 0.25

    def set_pair(self, pair):
        self._pair = pair

    def query(self, method, values={}):
        if method in self.public:
            url = 'https://bittrex.com/api/v1.1/public/'
        elif method in self.market:
            url = 'https://bittrex.com/api/v1.1/market/'
        elif method in self.account:
            url = 'https://bittrex.com/api/v1.1/account/'
        else:
            return 'Something went wrong, sorry.'

        url += method + '?' + urllib.urlencode(values)

        if method not in self.public:
            url += '&apikey=' + self.key
            url += '&nonce=' + str(int(time.time()))
            signature = hmac.new(self.secret, url, hashlib.sha512).hexdigest()
            headers = {'apisign': signature}
        else:
            headers = {}

        req = urllib2.Request(url, headers=headers)
        response = json.loads(urllib2.urlopen(req).read())

        if response["result"]:
            return response["result"]
        else:
            return response["message"]

    def get_markets(self):
        return self.query('getmarkets')

    def get_currencies(self):
        return self.query('getcurrencies')

    def get_ticker(self, market):
        return self.query('getticker', {'market': market})

    def get_market_summaries(self):
        return self.query('getmarketsummaries')

    @errorHandler('Failed to Query Bittrex API, Restarting Loop')
    def get_market_summary(self, market):
        return self.query('getmarketsummary', {'market': market})

    def _get_orderbook(self, market, order_type, depth=20):
        return self.query('getorderbook', {
            'market': market,
            'type': order_type,
            'depth': depth
        })

    @errorHandler('Failed to get Bittrex Asks, skipping order attempt')
    def _get_top_order(self, market, order_type):
        orderbook = self._get_orderbook(market, order_type)
        return orderbook[0]["Quantity"]

    def get_lowest_ask_size(self):
        if not self._pair:
            return False
        currencyPair = self._pair
        return self._get_top_order(currencyPair, 'sell')

    def get_highest_bid_size(self):
        if not self._pair:
            return False
        currencyPair = self._pair
        return self._get_top_order(currencyPair, 'buy')

    def get_lowest_ask_price(self):
        if not self._pair:
            return False
        summary = self.get_market_summary(self._pair)
        if not summary:
            return False
        return summary[0]['Ask']

    def get_highest_bid_price(self):
        if not self._pair:
            return False
        summary = self.get_market_summary(self._pair)
        if not summary:
            return False
        return summary[0]['Bid']

    def get_market_history(self, market, count=20):
        return self.query('getmarkethistory', {
            'market': market,
            'count': count
        })

    def buy_limit(self, quantity, rate):
        if not self._pair:
            return False
        market = self._pair

        return self.query('buylimit', {
            'market': market,
            'quantity': quantity,
            'rate': rate
        })

    def buy_market(self, market, quantity):
        return self.query('buymarket', {
            'market': market,
            'quantity': quantity
        })

    def sell_limit(self, quantity, rate):
        if not self._pair:
            return False
        market = self._pair

        return self.query('selllimit', {
            'market': market,
            'quantity': quantity,
            'rate': rate
        })

    def sell_market(self, market, quantity):
        return self.query('sellmarket', {
            'market': market,
            'quantity': quantity
        })

    def cancel(self, uuid):
        return self.query('cancel', {'uuid': uuid})

    def get_open_orders(self, market):
        return self.query('getopenorders', {'market': market})

    def get_balances(self):
        return self.query('getbalances')

    @errorHandler('Failed to Query Bittrex for Balance Data, Restarting Loop')
    def get_balance(self, currency):
        return self.query('getbalance', {'currency': currency})

    def get_deposit_address(self, currency):
        return self.query('getdepositaddress', {'currency': currency})

    def withdraw(self, currency, quantity, address):
        return self.query('withdraw', {
            'currency': currency,
            'quantity': quantity,
            'address': address
        })

    def get_order(self, uuid):
        return self.query('getorder', {'uuid': uuid})

    def get_order_history(self, market, count):
        return self.query('getorderhistory', {
            'market': market,
            'count': count
        })

    def get_withdrawal_history(self, currency, count):
        return self.query('getwithdrawalhistory', {
            'currency': currency,
            'count': count
        })

    def get_deposit_history(self, currency, count):
        return self.query('getdeposithistory', {
            'currency': currency,
            'count': count
        })
