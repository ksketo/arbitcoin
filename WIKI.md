# Arbitrage Example

**Reference**: https://steemit.com/arbitrage/@kesor/the-math-behind-cross-exchange-arbitrage-trading

Assume BTC is the base currency and ETH the target currency

- **x**: amount of BTC used to buy ETH
- **y**: amount of ETH bought
- **p**: price in BTC at the `buy low` exchange
- **q**: price in BTC at the `sell high` exchange
- **f**: fee % at the `buy low` exchange
- **g**: fee % at the `sell high` exchange
- **δ**: amount of BTC gained by the arbitrage

## Step 1

**Buy ETH using BTC in the exchange with the [low ask price ETH]**
`(x/p)(1-f)=y`

## Step 2

**Sell ETH for BTC in the exchange with the [high bid price BTC]**
`yq(1-g)=x+δ`

## Calculations

Using the two equations we can derive the formula for the profit

`δ = x[(q/p)(1-f)(1-g) - 1]`

Which is equivalent to

`δ = tradesize * [arbitrage * (1-fees)^2 - 1]`

So in order to have profit of 0.5% per trade, for fees 0.25% we need arbitrage opportunity of **> 1%**.

***Note***
The profits can potentially increase by selling larger amount of ETH than `y`.

### Cost for rebalancing

The above calculations didn't consider the cost for rebalancing the portfolio in order to have liquidity. There are two ways to rebalance.
A - Wait for the spread to close and trade back
B - Transfer balances between exchanges


### (A) - Closed spread rebalancing

the profit formula becomes

### (B) - Withdrawal rebalancing

**Bittrex withdrawal fees**
- *ETH*: 0.006
- *BTC*: 0.001
- *SC*:

**Poloniex withdrawal fees**
- *ETH*: 0.01
- *BTC*: 0.0005
- *SC*: 0.1

For **tf_eth** and **tf_btc** *ETH* and *BTC* withdrawal fees respectively, where

`tf_ethinbtc = p * tf_eth , approximately`

the profit formula now becomes

`δ = tradesize * [arbitrage * (1-fees)^2 - 1] - tf_btc - p * tf_eth`

and the minimum arbitrage

\[
arb \geq \frac{1}{(1-f) \cdot (1-g)} \cdot \bigg[\frac{tf_{base} + p \cdot tf_{target}}{tradesize} + 1 + a\bigg]
\]

where `a` is the profit we want to get.

**Profitability example**

In order to make a profit of 0.5% of the initial investment, assuming `p = 0.1` the arbitrage needs to satisfy the following relationship:

```
tradesize * [arbitrage * (1-0.0025)^2 - 1] - 0.0016 >= 0.005 * tradesize =>
tradesize * [arbitrage * 0.995 - 1.005] >= 0.0016
```

which for `tradesize = 1 BTC` gives minimum arbitrage opportunity

`arbitrage >= 1.0116`
