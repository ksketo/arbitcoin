#!/usr/bin/env python
import logging
import argparse
import time
import sys

from exchanges.poloniex import Poloniex as poloniex
from exchanges.bittrex import Bittrex as bittrex

try:
    # For Python 3+
    from configparser import ConfigParser, NoSectionError
except ImportError:
    # Fallback to Python 2.7
    from ConfigParser import ConfigParser, NoSectionError


class ArbitBot(object):
    def __init__(self):
        self._ask = None
        self._pair = None
        self.sell_exchange = None
        self.buy_exchange = None

        self.setup_arg_parser()
        self.setup_logger()
        self.load_config()

    def set_pair(self, pair):
        self._pair = pair

    def setup_arg_parser(self):
        """Set up argument parser"""

        parser = argparse.ArgumentParser(
            description='Poloniex/Bittrex Arbitrage Bot')
        parser.add_argument(
            '-s',
            '--symbol',
            default='XMR',
            type=str,
            required=False,
            help='symbol of your target coin [default: XMR]')
        parser.add_argument(
            '-b',
            '--basesymbol',
            default='BTC',
            type=str,
            required=False,
            help='symbol of your base coin [default: BTC]')
        parser.add_argument(
            '-r',
            '--rate',
            default=1.01,
            type=float,
            required=False,
            help='min arbitrage, 1.01 is 1 % price difference [default: 1.01]'
        )
        parser.add_argument(
            '-m',
            '--max',
            default=0.0,
            type=float,
            required=False,
            help='max order size in target currency (def 0.0 is unlimited)'
        )
        parser.add_argument(
            '-i',
            '--interval',
            default=1,
            type=int,
            required=False,
            help='seconds to sleep between loops [default: 1]')
        parser.add_argument(
            '-c',
            '--config',
            default='arbbot.conf',
            type=str,
            required=False,
            help='config file [default: arbbot.conf]')
        parser.add_argument(
            '-l',
            '--logfile',
            default='arbbot.log',
            type=str,
            required=False,
            help='file to output log data to [default: arbbot.log]')
        parser.add_argument(
            '-d',
            '--dryrun',
            action='store_true',
            required=False,
            help='simulates without trading (API keys not required)')
        parser.add_argument(
            '-v',
            '--verbose',
            action='store_true',
            required=False,
            help='enables extra console messages (for debugging)')
        self.args = parser.parse_args()

    def setup_logger(self):
        """Set up bot logger"""

        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        # Create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        # Create file handler and set level to debug
        fh = logging.FileHandler(
            self.args.logfile, mode='a', encoding=None, delay=False)
        fh.setLevel(logging.DEBUG)
        # Create formatter
        formatter = logging.Formatter(
            '%(asctime)s - %(levelname)s - %(message)s')
        # Add formatter to handlers
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # Add handlers to logger
        self.logger.addHandler(ch)
        self.logger.addHandler(fh)

    def load_config(self):
        """Load bot configuration"""

        config = ConfigParser()
        try:
            config.read(self.args.config)
            polo_key = config.get('ArbBot', 'poloniexKey')
            polo_secret = config.get('ArbBot', 'poloniexSecret')
            btrx_key = config.get('ArbBot', 'bittrexKey')
            btrx_secret = config.get('ArbBot', 'bittrexSecret')
        except NoSectionError:
            self.logger.warning('No Config File Found! Running in Drymode!')
            self.args.dryrun = True
            polo_key = 'POLONIEX_API_KEY'
            polo_secret = 'POLONIEX_API_SECRET'
            btrx_key = 'BITTREX_API_KEY'
            btrx_secret = 'BITTREX_API_SECRET'
            config.add_section('ArbBot')
            config.set('ArbBot', 'poloniexKey', polo_key)
            config.set('ArbBot', 'poloniexSecret', polo_secret)
            config.set('ArbBot', 'bittrexKey', btrx_key)
            config.set('ArbBot', 'bittrexSecret', btrx_secret)
        try:
            with open(self.args.config, 'w') as configfile:
                config.write(configfile)
        except IOError:
            self.logger.error('Failed to create and/or write to {}'.format(
                self.args.config))

        self.exchange_config = {
            'poloniexKey': polo_key,
            'poloniexSecret': polo_secret,
            'bittrexKey': btrx_key,
            'bittrexSecret': btrx_secret
        }

    def quit(self):
        self.logger.info('KeyboardInterrupt, quitting!')
        sys.exit()

    def get_tradesize(self, sellbook, buybook, sell_balance, buy_balance):
        tradesize = min(sellbook, buybook)

        # Setting order size incase balance not enough
        if sell_balance < tradesize:
            self.logger.info('Tradesize ({}) larger than sell balance ({} @ {}), lowering tradesize.'.format(tradesize, sell_balance, self.sell_exchange.name))
            tradesize = sell_balance

        if (tradesize * self._ask) > buy_balance:
            new_tradesize = buy_balance / self._ask
            self.logger.info('Tradesize ({}) larger than buy balance ({} @ {}), lowering tradesize to {}.'.format(tradesize, buy_balance, self.buy_exchange.name, new_tradesize))
            tradesize = new_tradesize

        if self.args.max >= 0.0 and tradesize > self.args.max:
            self.logger.debug('Tradesize ({}) larger than maximum ({}), lowering tradesize.'.format(tradesize, self.args.max))
            tradesize = self.args.max

        return tradesize

    def trade(self, ask, bid, sell_balance, buy_balance):
        """Place buy and sell orders on Polo and Bittrex

        This function takes the exchange from which to buy, buys the defined
        amount of coins and sells in the other. Currently works between
        Poloniex and Bittrex.
        Before placing the orders it will verify that the amount of coins to
        sell is available in the account and the amount to buy is larger then
        the coins*price of highest bid.

        Args:
            ask:         Lowest ask price of order book in exchange to buy from
            bid:         Highest bid price of order book in exchange to sell at
            sellBalance: Amount to sell - has to be available in the account
            buyBalance:  Amount to buy - has to be larger then the coins*price
                         of highest bid
        """
        self._ask = ask
        self.arbitrage = bid / ask
        print('DEBUG: Current Rate: {} | Minimum Rate: {}'.format(
            self.arbitrage, self.args.rate))

        # Return if minumum arbitrage percentage is not met
        if self.arbitrage <= self.args.rate:
            return

        sellbook = self.buy_exchange.get_lowest_ask_size()
        buybook = self.sell_exchange.get_highest_bid_size()

        if (not sellbook or not buybook):
            return

        self.logger.info(
            'OPPORTUNITY: BUY @ ' + self.buy_exchange.name + ' | SELL @ ' +
            self.sell_exchange.name + ' | RATE: ' + str(self.arbitrage) + '%')

        # Find minimum order size
        tradesize = self.get_tradesize(sellbook, buybook, sell_balance,
                                       buy_balance)

        # Check if above min order size
        if (tradesize * bid) > 0.0005001:
            self.logger.info("ORDER {}\nSELL: {}    | {} @ {:.8f} (Balance: {})\nBUY: {}    | {} @ {:.8f} (Balance: {})".format(self._pair, self.sell_exchange.name, tradesize, bid, sell_balance, self.buy_exchange.name, tradesize, ask, buy_balance))
            # Execute order
            if not self.args.dryrun:
                self.buy_exchange.buy_limit(tradesize, ask)
                self.sell_exchange.sell_limit(tradesize, bid)

                # Rebalance portfolio if tradesize is large part of the
                # portfolio
                if (tradesize > sell_balance / 2) or (tradesize * self._ask > buy_balance / 2):
                    self.rebalance(tradesize)
            else:
                self.logger.info('Dryrun: skipping order')
        else:
            self.logger.warning(
                'Order size not above min order size, no trade was executed')

    def rebalance(self, tradesize, rb_type="spread"):
        """Rebalance portfolio on Polo and Bittrex

        Args:
            tradesize:
            type:
        """

        if rb_type == 'spread':
            # get bid at buy_exchange
            bid = self.buy_exchange.get_highest_bid_price()
            # get ask at sell_exchange
            ask = self.sell_exchange.get_lowest_ask_price()

            # While the spread between the two currencies is large repeat
            spread_closed = bid / ask >= 1 + (self.arbitrage - 1) / 6
            while not (ask and bid and spread_closed):
                bid = self.buy_exchange.get_highest_bid_price()
                ask = self.sell_exchange.get_lowest_ask_price()

            # Sell at buy_exchange
            self.buy_exchange.sell_limit(tradesize, bid)
            # Buy at sell_exchange
            self.sell_exchange.buy_limit(tradesize, ask)
        elif rb_type == 'withdrawal':
            # TODO
            self.logger.info(
                'Withdrawal rebalancing method has not been implemented yet.')


def main():
    """Main function to run"""

    # Initialize ArbitBot
    arbbot = ArbitBot()

    # Setup Argument Parser
    args = arbbot.args

    # Create Logger
    logger = arbbot.logger

    # Load Config File
    config = arbbot.exchange_config
    polo_key = config['poloniexKey']
    polo_secret = config['poloniexSecret']
    btrx_key = config['bittrexSecret']
    btrx_secret = config['bittrexSecret']

    # Pair Strings for accessing API responses
    target_currency = args.symbol
    base_currency = args.basesymbol
    btrx_pair = '{0}-{1}'.format(base_currency, target_currency)
    polo_pair = '{0}_{1}'.format(base_currency, target_currency)
    arbbot.set_pair(btrx_pair)

    # Create Exchange API Objects
    btrx_api = bittrex(btrx_key, btrx_secret)
    btrx_api.set_pair(btrx_pair)
    polo_api = poloniex(polo_key, polo_secret)
    polo_api.set_pair(polo_pair)

    # Log Startup Settings
    logger.info(
        'Arb Pair: {} | Rate: {} | Interval: {} | Max Order Size: {}'.format(
            btrx_pair, args.rate, args.interval, args.max))
    if args.dryrun:
        logger.info("Dryrun Mode Enabled (will not trade)")

    # Main Loop
    while True:
        # Query Poloniex Prices
        current_values = polo_api.get_ticker()
        polo_bid = float(current_values[polo_pair]["highestBid"])
        polo_ask = float(current_values[polo_pair]["lowestAsk"])

        # Query Bittrex Prices
        summary = btrx_api.get_market_summary(btrx_pair)
        btrx_ask = summary[0]['Ask']
        btrx_bid = summary[0]['Bid']

        if (not current_values or not summary):
            continue

        # Print Prices
        print(
            '\nPair: {}'.format(btrx_pair) +
            '\nASKS: Poloniex @ {:.8f}'.format(polo_ask) +
            ' | {:.8f} @ Bitrex'.format(btrx_ask) +
            '\nBIDS: Poloniex @ {:.8f}'.format(polo_bid) +
            ' | {:.8f} @ Bitrex'.format(btrx_bid))

        # Get Balance Information, fake numbers if dryrun.
        if not args.dryrun:
            # Query Bittrex API
            btrx_target_balance = btrx_api.get_balance(target_currency)
            btrx_base_balance = btrx_api.get_balance(base_currency)

            # Query Poloniex API
            polo_balance_data = polo_api.get_balances()
            polo_target_balance = polo_balance_data[target_currency]
            polo_base_balance = polo_balance_data[base_currency]

            if (not btrx_target_balance or not btrx_base_balance
                    or not polo_base_balance):
                continue
        else:
            # Faking balance numbers for dryrun simulation
            btrx_target_balance = 100.0
            btrx_base_balance = 100.0
            polo_target_balance = 100.0
            polo_base_balance = 100.0

        if polo_ask < btrx_bid:
            # Buy from Polo, Sell to Bittrex
            arbbot.buy_exchange = polo_api
            arbbot.sell_exchange = btrx_api
            arbbot.trade(polo_ask, btrx_bid, btrx_target_balance,
                         polo_base_balance)
        elif btrx_ask < polo_bid:
            # Sell to Polo, Buy from Bittrex
            arbbot.buy_exchange = btrx_api
            arbbot.sell_exchange = polo_api
            arbbot.trade(btrx_ask, polo_bid, polo_target_balance,
                         btrx_base_balance)

        time.sleep(args.interval)


if __name__ == "__main__":
    main()
